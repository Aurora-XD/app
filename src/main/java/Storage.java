
import java.util.HashMap;
import java.util.Map;

public class Storage {

    private Map<Ticket,Luggage> myStorage = new HashMap<>();

    private int number;

    private Assistant assistant;

    public Storage(Map<Ticket, Luggage> myStorage) {
        this.myStorage = myStorage;
    }

    public Storage() {
        this(10);
    }

    public Storage(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }


    public Ticket save(Luggage luggage) {

        if(this.myStorage.size() >= this.number){
            throw new RuntimeException("this is runtime exception");
        }
        Ticket ticket = new Ticket();
        myStorage.put(ticket,luggage);
        return ticket;
    }

    public Ticket save(Luggage luggage,Ticket ticket) {
        myStorage.put(ticket,luggage);
        return ticket;
    }

    public Luggage get(Ticket ticket) {
        return myStorage.get(ticket);
    }

    public Luggage get(Ticket ticket,int id) {
        return myStorage.get(ticket);
    }
}
