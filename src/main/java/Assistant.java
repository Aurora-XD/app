import java.util.HashMap;
import java.util.Map;

public class Assistant {

    private Storage storage;

    public Assistant(Storage storage) {
        this.storage = storage;
    }

    public Ticket save(Luggage luggage) {
        return  storage.save(luggage);
    }

    public Ticket save(Luggage luggage,Ticket ticket) {
        return storage.save(luggage,ticket);
    }

    public Luggage get(Ticket ticket) {
        return storage.get(ticket);
    }
}
