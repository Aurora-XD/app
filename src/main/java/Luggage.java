import java.util.Objects;

public class Luggage {

    private int id;

    private String name;

    public Luggage() {
    }

    public Luggage(String name) {
        this.name = name;
    }

    public Luggage(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Luggage luggage = (Luggage) o;
        return id == luggage.id &&
                Objects.equals(name, luggage.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
