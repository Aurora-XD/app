import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

public class AppAssistantTest {

    private Storage storage;
    private Assistant assistant;

    @BeforeEach
    void setUp() {
        storage = new Storage();
        assistant = new Assistant(storage);
    }

    @Test
    void should_save_by_assistant() {

        Luggage firstPackage = new Luggage("First Package");

        Ticket ticket = assistant.save(firstPackage);

        assertNotNull(ticket);
    }

    @Test
    public void should_return_package_by_id() {

        Luggage firstPackage = new Luggage("My Package");

        Ticket ticket = assistant.save(firstPackage);

        assertEquals("My Package",assistant.get(ticket).getName());

    }

    @Test
    public void should_return_null_when_package_is_empty() {
        Luggage firstPackage = new Luggage();

        Ticket ticket = assistant.save(firstPackage);

        assertNull(assistant.get(ticket).getName());

    }

    @Test
    public void should_return_first_when_has_two_package() {
        Luggage firstPackage1 = new Luggage(1,"first");
        Luggage firstPackage2 = new Luggage(2,"second");

        Ticket ticket1 = assistant.save(firstPackage1);

        assistant.save(firstPackage2,ticket1);

        assertEquals("second",assistant.get(ticket1).getName());

    }

    @Test
    public void should_throw_exception_when_number_is_zero() {
        Storage storage = new Storage(1);

        Assistant assistant1 = new Assistant(storage);
        Luggage firstPackage1 = new Luggage("first");
        Ticket ticket1 = assistant1.save(firstPackage1);
        Luggage firstPackage2 = new Luggage("second");

        try{
            assistant1.save(firstPackage2);

        }catch (RuntimeException err){
            assertEquals(RuntimeException.class,err.getClass());
            assertEquals("this is runtime exception",err.getMessage());
        }


    }


    @Test
    void should_throw_RuntimeException_use_mock() {
        Storage mock = mock(Storage.class);
        Assistant assistant = new Assistant(mock);

        Luggage firstPackage1 = new Luggage("first");

        when(assistant.save(firstPackage1)).thenThrow(new RuntimeException("exception"));

        try {
            assistant.save(firstPackage1);
        }catch (RuntimeException err){
            assertEquals(RuntimeException.class,err.getClass());
            assertEquals("exception",err.getMessage());
        }

    }
}
