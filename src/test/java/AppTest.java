
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class AppTest {

    @Test
    public void should_return_a_ticket() {

        Storage storage = new Storage();
        Luggage firstPackage = new Luggage("First Package");

        Ticket ticket = storage.save(firstPackage);

        assertNotNull(ticket);

    }


    @Test
    public void should_return_package_by_id() {
        Storage storage = new Storage();
        Luggage firstPackage = new Luggage("My Package");

        Ticket ticket = storage.save(firstPackage);

        assertEquals("My Package",storage.get(ticket).getName());

    }

    @Test
    public void should_return_null_when_package_is_empty() {
        Storage storage = new Storage();
        Luggage firstPackage = new Luggage();

        Ticket ticket = storage.save(firstPackage);

        assertNull(storage.get(ticket).getName());

    }

    @Test
    public void should_return_first_when_has_two_package() {
        Storage storage = new Storage();
        Luggage firstPackage1 = new Luggage(1,"first");
        Luggage firstPackage2 = new Luggage(2,"second");

        Ticket ticket1 = storage.save(firstPackage1);
        storage.save(firstPackage2,ticket1);

        assertEquals("second",storage.get(ticket1).getName());

    }

    @Test
    public void should_throw_exception_when_number_is_zero() {
        Storage storage = new Storage(1);
        Luggage firstPackage1 = new Luggage(1,"first");
        Ticket ticket1 = storage.save(firstPackage1);
        Luggage firstPackage2 = new Luggage(2,"second");

        assertThrows(RuntimeException.class,()->{storage.save(firstPackage2);});
    }

}
